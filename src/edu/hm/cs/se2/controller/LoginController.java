package edu.hm.cs.se2.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Flo on 08.04.16.
 */
@Controller
public class LoginController {
    @RequestMapping(value = "/login.form", method = RequestMethod.GET)
    public ModelAndView showLogin(HttpSession session, HttpServletRequest request) {
        ModelAndView mv = new ModelAndView("login");
        return mv;
    }
}
